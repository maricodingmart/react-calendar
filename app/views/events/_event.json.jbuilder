json.extract! event, :id
json.start event.start.nil? ? nil : event.start.to_datetime
json.end event.end.nil? ? nil :  event.end.to_datetime
json.title event.title.nil? ? nil :  event.title
