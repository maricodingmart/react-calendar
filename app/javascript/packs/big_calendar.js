import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import BigCalendar from 'react-big-calendar';
import moment from 'moment';
import { Component } from 'react';

BigCalendar.momentLocalizer(moment); // or globalizeLocalizer


const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/;

  function reviver(key, value) {
      if (typeof value === "string" && dateFormat.test(value)) {
          return new Date(value);
      }

      return value;
  }

export default class MyCalendar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      events: this.props.calendar.events,
      event_id:null,
      event_image: "https://calendar-cleartrip-marimanohar.s3.ap-south-1.amazonaws.com/calendar/Screenshot+2020-02-19+at+11.09.44+AM.png",
      event_start_on: null,
      event_end_on:null,
      event_title: null
    };
  }
  render() {
    return(
      <div>
        <BigCalendar
          events={this.state.events}
          defaultView='month'
          views={['week','month','day']}
          selectable
          step={60}
          timeevents={1}
          onSelectEvent={event => this.openModal(event)}
          onSelectSlot={(slotInfo) => this.openModal(slotInfo)}
            />
          <div className="modal fade" id="event-form">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  <p>from : {this.state.event_start_on ? this.state.event_start_on.toLocaleString():''}</p>
                  <p>to : {this.state.event_end_on ? this.state.event_end_on.toLocaleString():''}</p>

                  <form>
                   <label>
                      Title
                    </label>
                    <input type="text" name="title" onChange={this.handleInputChange} value={this.state.event_title} />
                    <input type="file" name="image" onChange={this.handleImageChange} />
                    <img src={this.state.event_image} width="70%" height="70%"/>
                  </form>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-success" onClick={this.createEvent}>Save</button>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
  createEvent = (element) => {
    Rails.ajax({
      type: 'POST',
      data: 'event[id]='+this.state.event_id+'&event[start]='+this.state.event_start_on+'&event[end]='+this.state.event_end_on+'&event[title]='+this.state.event_title+'&event[image]='+this.state.event_image,
      url: '/events',
      success: (element) => {
        var answer = JSON.parse(JSON.stringify(element), reviver);
        this.setState({ events:answer.events})
      }
    });
    this.toggleModal()
  }

   handleInputChange = (e) => {
    this.setState({
      event_title: e.target.value
    })
  }


  handleImageChange = (e) => {
    this.setState({
      event_image: URL.createObjectURL(e.target.files[0])
    })
  }


  openModal = (element) => {
    debugger
    this.setState({
      event_id: element.id ? element.id : null,
      event_start_on: element.start,
      event_end_on:element.end,
      event_title: element.title ? element.title : null
    })
    this.toggleModal()
  }

  toggleModal = () => {
    const form = $('#event-form');
    form.modal('toggle');
  }

}


const eventsContainer = document.getElementById('events');
if (eventsContainer) {
  debugger
  const calendar = JSON.parse(eventsContainer.dataset.events, reviver);
  console.log(calendar.events);
  ReactDOM.render(
    <MyCalendar calendar= {calendar}/>
    , eventsContainer);
}


// scroll to see time.now()
const d = new Date();
const n = d.getHours();

document.getElementsByClassName('rbc-time-content')[0].scrollTop = ((n-1)*40);